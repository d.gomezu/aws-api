const express = require('express');
const fs = require('fs');
const http = require('http');
const https = require('https');
require('dotenv').config();


const app = express();

const httpsServerOptions = {
    key: fs.readFileSync(process.env.KEY_PATH),
    cert: fs.readFileSync(process.env.CERT_PATH)
};

const serverHttp = http.createServer(app);
serverHttp.listen(process.env.HTTP_PORT, process.env.IP_SERVER);

const serverHttps = https.createServer(httpsServerOptions, app);
serverHttps.listen(process.env.HTTPS_PORT, process.env.IP_SERVER);

app.use((req, res, next) => {
    if (req.secure) {
        next();
    } else {
        res.redirect(`https://${req.headers.host}${req.url}`);
    }
});

app.get('/api/random', function (req, res) {
    let min = 1;
    let max = 100;
    let randomValue = Math.floor(Math.random() * (max - min)) + min;

    res.json({ random: randomValue });
});
